> a demo showing an alternative approach for overriding env vars in
> docker-compose.

# Requirements

`docker-compose` already has support for a `.env` file but I want that to
contain all the *default* values and to be part of version control. I need a way
for a user to supply their own config.

I'm working on a project that will be shipped as a docker-compose stack. The
images are all built and deployed to container repos so the system will be
"distributed" by cloning a git repo that contains docker-compose files and just
enough tooling (a bash script in this case) to make it user friendly to operate.

So, the requirements I have:

  1. use convention-over-configuration to minimise config. Ideally no config is
     required to launch and then users only need to override a value when they
     want to change something.
  1. user supplied config must be excluded from version control
  1. editing files in version control is not allowed. Well of course it is, but
     that makes upgrades via `git pull` harder so we want to avoid it.
  1. the system has the chance to catch when mandatory config items are NOT
     supplied. This is the main reason we have the user run our bash script and
     not directly run docker-compose
  1. upgrading is as painless as possible. Approaches like "copy the default
     config file" are out because then future changes are shadowed by the copied
     file. Only overrides should live in the user config
  1. a vanila unix-ey system can run it. So no installing python dependencies
     like `milc`. Bash really shines here
  1. the files are all text based so they version control well, so no binaries
     in version control
  1. repeatability: I want config to live in files so your command is as short
     as possible. Recalling a command from history with a heap of param is out.
  1. the ability to pass config to our wrapper script, not to docker-compose

# How to run
  1. clone the repo
  1. do your first run with all the default values. `VAL3` is the focus of this
     demo, and for this first run we see it has the value from `.env`
      ```bash
      $ ./doit
      VAL1=the-default
      VAL2=first
      VAL3=first
      ```
  1. now we decide to override `VAL3` so we create a file (that's ignored from
     version control):
      ```bash
      $ echo "VAL3=second" > local.env
      $ ./doit
      VAL1=the-default
      VAL2=first
      VAL3=second
      ```
  1. you can see that `VAL3` has our overriden value. This approach satisfies all
     the requirements.
  1. let's also demo overriding a config item that our bash script uses, not the
     docker-compose stack:
      ```bash
      $ echo "DOIT_ENABLE_FEATURE1=1" >> local.env
      $ ./doit
      [INFO] feature 1 enabled!
      sh: line 3: VAL4: you must provide this value in local.env
      ```
  1. oh, it seems when we enable this feature, we must also provide `VAL4`.
     Let's do that:
      ```bash
      $ echo "VAL4=whatever" >> local.env
      $ ./doit
      [INFO] feature 1 enabled!
      ...
      ```
